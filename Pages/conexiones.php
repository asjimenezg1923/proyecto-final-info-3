<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"> 
	<link rel="stylesheet" href="../css/estiloconexiones.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="icon" href="https://image.flaticon.com/icons/png/512/1086/1086719.png">
    <title>Conexiones</title>
</head>
<body>
    
<?php 
   /* $mysqli = new mysqli("localhost", "id17124430_santi1923", "Alfaveto1923*", "id17124430_datoscar");
    if ($mysqli->connect_errno) {
        echo "Fallo al conectar a MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
    }
    
    $mysqli->close();*/
    ?>

<div class="contenedor">
		<header class="header">
        <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark">
				<div class="container-fluid">
				  <img src="https://image.flaticon.com/icons/png/512/4892/4892143.png" alt="" width="40px" style="margin-right:20px;" class="d-inline-block align-text-top">
				  <a class="navbar-brand" >Conexiones</a>
				  <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				  </button>
				  <div class="collapse navbar-collapse" id="navbarNavDropdown">
					<ul class="navbar-nav">
					  <li class="nav-item">
						<a class="nav-link" aria-current="page" href="../index.php">Inicio</a>
					  </li>
					  <li class="nav-item">
						<a class="nav-link active"  href="../Pages/conexiones.php">Conexiones</a>
					  </li>
					  <li class="nav-item">
						<a class="nav-link" href="../Pages/sensores.php">Sensores</a>
					  </li>
					  
					  <li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle"  id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
						  Montajes
						</a>
						<ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
						  <li><a class="dropdown-item" href="../Pages/MontajeUno.php">Montaje Arduino Uno</a></li>
						  <li><a class="dropdown-item" href="../Pages/MontajeMega.php">Montaje Arduino Mega</a></li>
						</ul>
					  </li>
					  <li class="nav-item">
						<a class="nav-link" href="../Pages/controles.php">Control Web</a>
					  </li>
					</ul>
				  </div>
				</div>
			  </nav>
		</header>
		<main class="contenido">
		     
		    
        <table class="table table-dark table-striped table-hover">
            <thead>
                <tr>
                    <th scope="col">#</th>
                   <th scope="col-2">Estado</th> 
                    <th scope="col">Hora</th>
                    
                </tr>
            </thead>
            <tbody>
                <?php
                    $mysqli = new mysqli("localhost", "id17124430_santi1923", "Alfaveto1923*", "id17124430_datoscar");
                    if ($mysqli->connect_errno) {
                    echo "Fallo al conectar a MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
                    }
                
                $sql="SELECT ARDUINO.ID,ARDUINO.comando,ARDUINO.hora FROM ARDUINO";
                $consulta=mysqli_query($mysqli,$sql);
                while($dato = mysqli_fetch_array($consulta)) {
                ?>
                <tr>
                     <th scope="row"> <?php echo $dato['ID']; ?> </th>
                     <td> <?php echo $dato['comando']; ?> </td>
                     <td> <?php echo $dato['hora']; ?> </td>
                </tr> 
            
                <?php    
                    }            
                $mysqli->close();
            ?>
            </tbody>
                </table>
                
		</main>
		
		
		<footer class="footer">
		<div class="names">
				<div class="left">
				<h2>Estudiantes:</h2>
				<ul>
					<li>Jhasley Nathalia Pinzón Saíz</li>
					<li>Faiber Rivera Mateus</li>
					<li>Andrés Santiago Jiménez Guzmán</li>
				</ul>
				</div>
				<div class="right">
					<img src="https://image.flaticon.com/icons/png/512/3662/3662920.png" alt="" width="150px">
				</div>
				
			</div>
		</footer>
	</div>
    <button class="arrow-up">
	<i class="fa fa-chevron-up"></i>
  </button>
  <script src="../js/arrowup.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
</body>
</html>