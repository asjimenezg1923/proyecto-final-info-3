<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"> 
	<link rel="stylesheet" href="../css/control.css">
    <link rel="stylesheet" href="../css/flechitas.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
	<link rel="icon" href="https://image.flaticon.com/icons/png/512/808/808439.png">
	<title>Control Web</title>
</head>
<body>
	<div class="contenedor">
		<header class="header">
        <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark">
				<div class="container-fluid">
				  <img src="https://image.flaticon.com/icons/png/512/1417/1417651.png" alt="" width="40px" style="margin-right:20px;" class="d-inline-block align-text-top">
				  <a class="navbar-brand" >Controles Web</a>
				  <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				  </button>
				  <div class="collapse navbar-collapse" id="navbarNavDropdown">
					<ul class="navbar-nav">
					  <li class="nav-item">
						<a class="nav-link" aria-current="page" href="../index.php">Inicio</a>
					  </li>
					  <li class="nav-item">
						<a class="nav-link"   href="../Pages/conexiones.php">Conexiones</a>
					  </li>
					  <li class="nav-item">
						<a class="nav-link" href="../Pages/sensores.php">Sensores</a>
					  </li>
					  
					  <li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle"  id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
						  Montajes
						</a>
						<ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
						  <li><a class="dropdown-item" href="../Pages/MontajeUno.php">Montaje Arduino Uno</a></li>
						  <li><a class="dropdown-item" href="../Pages/MontajeMega.php">Montaje Arduino Mega</a></li>
						</ul>
					  </li>
					  <li class="nav-item">
						<a class="nav-link active" href="../Pages/controles.php">Control Web</a>
					  </li>
					</ul>
				  </div>
				</div>
			  </nav>
		</header>
		<main class="contenido">
        <center><h1 class="Control">Controles WebSite</h1> </center>
             

<div class="d-flex justify-content-center" style="margin-right:110px;">
<div class="p-2 bd-highlight">

                  <div class="Arrows" style="transform:scale(0.7);">
                  <div>
  
                  </div>
                      <div>
                      <ul id="ul1">
                              <li id="up">
                              <div class="meh m1"><pre> </pre></div>
                              <div class="meh m2"><pre> </pre></div>
                              <div class="meh m3"><pre> </pre></div>
                              </li>
                              </ul>
                      
                      
                      </div>
                      <div></div>
                      <div>
                      <ul id="ul2">
                      <li id="left">
                          <div class="leh m1"><pre> </pre></div>
                          <div class="leh m2"><pre> </pre></div>
                          <div class="leh m3"><pre> </pre></div>
                      </li>
                      </ul>
                      </div>
                      <div>
                      <ul id="ul2">
                      <li id="down">
                          <div class="deh m1"><pre> </pre></div>
                          <div class="deh m2"><pre> </pre></div>
                          <div class="deh m3"><pre> </pre></div>
                      </li>
  
                      </ul>
  
                      </div>
                      <div>
                      <ul id="ul2">
                      <li id="right">
                          <div class="reh m1"><pre> </pre></div>
                          <div class="reh m2"><pre> </pre></div>
                          <div class="reh m3"><pre> </pre></div>
                      </li>
  
                      </ul>
  
                      </div>
                      <div class="item1" id="stop">
                          
                      </div>
                      </div>
                     
</div>
  <div class="p-2 bd-highlight"></div>
  <div class="p-2 bd-highlight"><img src="../Images/comandos.jpg" alt="..." width="450px" height="450px" style="margin-top:70px;"></div>
</div>          
                    
		</main>
		
		<footer class="footer">
        <div class="names">
				<div class="left">
				<h2>Estudiantes:</h2>
				<ul>
					<li>Jhasley Nathalia Pinzón Saíz</li>
					<li>Faiber Rivera Mateus</li>
					<li>Andrés Santiago Jiménez Guzmán</li>
				</ul>
				</div>
				<div class="right">
					<img src="https://image.flaticon.com/icons/png/512/3662/3662920.png" alt="" width="150px">
				</div>
				
			</div>
		<div class="new_footer_top">
                
                <div class="footer_bg">
                    <div class="footer_bg_one"></div>
                    <div class="footer_bg_two"></div>
                </div>
            </div>
            <div class="footer_bottom">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-6 col-sm-7">
                            <p class="mb-0 f_400">© NOTGRAVITY 2021 All rights reserved.</p>
                        </div>
                    </div>
                </div>
            </div>
		</footer>
	</div>
	<button class="arrow-up">
	<i class="fa fa-chevron-up"></i>
  </button>
  <script src="../js/arrowup.js"></script>
    <script src="../js/arrows.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.0/jquery.min.js"></script>
</body>
</html>