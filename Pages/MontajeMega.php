<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"> 
	<link rel="stylesheet" href="../css/montajearduino.css">
	<link rel="stylesheet" href="../css/caracteristicas.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
	<link rel="icon" href="https://programarfacil.com/wp-content/uploads/2018/06/icono-arduino-02.png">
	<title>Montaje Arduino Mega</title>
</head>
<body>
	<div class="contenedor">
		<header class="header">
        <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark">
				<div class="container-fluid">
				  <img src="https://image.flaticon.com/icons/png/512/1470/1470181.png" alt="" width="40px" style="margin-right:20px;" class="d-inline-block align-text-top">
				  <a class="navbar-brand">Arduino Mega</a>
				  <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				  </button>
				  <div class="collapse navbar-collapse" id="navbarNavDropdown">
					<ul class="navbar-nav">
					  <li class="nav-item">
						<a class="nav-link" aria-current="page" href="../index.php">Inicio</a>
					  </li>
					  <li class="nav-item">
						<a class="nav-link" href="../Pages/conexiones.php">Conexiones</a>
					  </li>
					  <li class="nav-item">
						<a class="nav-link" href="../Pages/sensores.php">Sensores</a>
					  </li>
					  
					  <li class="nav-item dropdown">
						<a class="nav-link active dropdown-toggle" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
						  Montajes
						</a>
						<ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
						  <li><a class="dropdown-item" href="../Pages/MontajeUno.php">Montaje Arduino Uno</a></li>
						  <li><a class="dropdown-item" href="../Pages/MontajeMega.php">Montaje Arduino Mega</a></li>
						</ul>
					  </li>
                      <li class="nav-item">
						<a class="nav-link" href="../Pages/controles.php">Control Web</a>
					  </li>
					</ul>
				  </div>
				</div>
			  </nav>
		</header>
		<main class="contenido">
                <div class="montaje">
                    <center><h1>Montaje Arduno Mega</h1>
                    <div id="container">
                        <img src="../Images/Esquema2.jpg" alt="" class="Img1">
                    </div>
                    <p>Plano de Montaje</p></center>
                    </div>
            <div class="container0">
                <div class="box">
                    <div class="imgBox">
                        <img src="https://www.electronicoscaldas.com/2590-thickbox_default/sistema-de-desarrollo-arduino-mega-2560-r3-clone.jpg">
                    </div>
                    <div class="content">
                        <p> Placa Arduino Mega </p>
                        <p> Precio: $75.500 </p>
                    </div>
                </div>
                <div class="box">
                    <div class="imgBox">
                        <img src="https://www.steren.com.co/media/catalog/product/cache/b69086f136192bea7a4d681a8eaf533d/a/r/ard-335.jpg">
                    </div>
                    <div class="content">
                        <p> Protoboard mini </p>
                        <p> Precio: $2.000 </p>
                    </div>
                </div>
                <div class="box">
                    <div class="imgBox">
                        <img src="https://www.geekfactory.mx/wp-content/uploads/2013/06/modulo-l298n-puente-h-driver-motores.jpg">
                    </div>
                    <div class="content">
                        <p> Puente H L298N </p>
                        <p> Precio: $9.500 </p>
                    </div>
                </div>
                <div class="box">
                    <div class="imgBox">
                        <img src="https://cdn.shopify.com/s/files/1/0557/2945/products/Chasis3_x700.PNG?v=1547596836">
                    </div>
                    <div class="content">
                        <p> Chasis para carro de cuatro ruedas </p>
                        <p> Precio: $20.000 </p>
                    </div>
                </div>
                <div class="box">
                    <div class="imgBox">
                        <img src="https://electronilab.co/wp-content/uploads/2013/07/A1.jpg">
                    </div>
                    <div class="content">
                        <p> Ultrasonido HC-04 </p>
                        <p> Precio: $7.000 </p>
                    </div>
                </div>
                <div class="box">
                    <div class="imgBox">
                        <img src="https://http2.mlstatic.com/D_NQ_NP_870967-MCO43334546609_092020-O.webp">
                    </div>
                    <div class="content">
                        <p> Sensor de Temperatura y Humedad</p>
                        <p> Precio: $10.000 </p>
                    </div>
                </div>
                <div class="box">
                    <div class="imgBox">
                        <img src="https://suconel.com/wp-content/uploads/M%C3%B3dulo-Sensor-De-Luz-Ldr-SENL-1.jpg">
                    </div>
                    <div class="content">
                        <p> Modulo arduino LDR- Fotorresistencia </p>
                        <p> Precio: $6.000 </p>
                    </div>
                </div>
                <div class="box">
                    <div class="imgBox">
                        <img src="https://ventaspop-images.staticgnt.com/qz9an7HzjCdhr8XZRA2-YOUfiZI=/filters:quality(90):strip_exif()/files/products/487/2991/081691.jpg">
                    </div>
                    <div class="content">
                        <p> Modulo Bluetooth HC-05 </p>
                        <p> Precio: $22.500 </p>
                    </div>
                </div>
                <div class="box">
                    <div class="imgBox">
                        <img src="https://www.microkitselectronica.com/831-large_default/bateria-lipo-922859-37v-3000ma.jpg">
                    </div>
                    <div class="content">
                        <p> Pila lipo 3000 mA a 7.4v </p>
                        <p> Precio: $42.500 </p>
                    </div>
                </div>
                <div class="box">
                    <div class="imgBox">
                        <img src="https://directvoltage-e7b.kxcdn.com/wp-content/uploads/2017/08/P8230472.jpg">
                    </div>
                    <div class="content">
                        <p> Modulo de carga bateria lipo </p>
                        <p> Precio: $15.000 </p>
                    </div>
                </div>
                <div class="box">
                    <div class="imgBox">
                        <img src="https://images-na.ssl-images-amazon.com/images/I/61eUGBND5rL._AC_SL1100_.jpg">
                    </div>
                    <div class="content">
                        <p> Cargador 12V a 3A </p>
                        <p> Precio: $15.900 </p>
                    </div>
                </div>
                <div class="box">
                    <div class="imgBox">
                        <img src="https://cdn.shopify.com/s/files/1/0020/8027/6524/products/paquete_de_100_leds_difusos_5mm_varios_colores_mexico_jalisco_guadalajara_700x700.jpg?v=1593816653">
                    </div>
                    <div class="content">
                        <p> Leds de colores </p>
                        <p> Precio: $300 </p>
                    </div>
                </div>
                <div class="box">
                    <div class="imgBox">
                        <img src="https://dynamoelectronics.com/wp-content/uploads/2017/08/rgbled-1.png">
                    </div>
                    <div class="content">
                        <p> Leds RGB </p>
                        <p> Precio: $450 </p>
                    </div>
                </div>
                <div class="box">
                    <div class="imgBox">
                        <img src="https://sandorobotics.com/wp-content/uploads/2017/09/kit-40-jumpers-de-20cm-macho-hembra-3.jpg">
                    </div>
                    <div class="content">
                        <p> Jumpers </p>
                        <p> Precio: $7.000 40 unidades </p>
                    </div>
                </div>
                <div class="box">
                    <div class="imgBox">
                        <img src="https://www.sigmaelectronica.net/wp-content/uploads/2020/09/RAX1.4W-220-ohm-5-1.jpg">
                    </div>
                    <div class="content">
                        <p> Resistencias 330 ohms </p>
                        <p> Precio: $100 c/u </p>
                    </div>
                </div>
            </div>
            <iframe src=https://create.arduino.cc/editor/not_gravity/bd24f9c6-43c2-4a79-8ce6-53c491252e78/preview?embed style="height:510px;width:100%;margin:10px 0" frameborder=0></iframe>
            
            
            
            
            
            
            <div class="container">
  <div class="row">
    <div class="col">
      <iframe class="video" src="https://drive.google.com/file/d/1ZWCXpYobAeNjyHt2MsXAA5JJFWotHSJi/preview" width="640" height="480" allow="autoplay"></iframe>
      <center><p>Voice</p></center>
    </div>
    <div class="col">
      <iframe class="video" src="https://drive.google.com/file/d/1f1pH2_qNB2tl5dGkcybGbtwo7FkTArHa/preview" width="640" height="480" allow="autoplay"></iframe>
      <center><p>Internet</p></center>
    </div>
    <div class="col">
      <iframe class="video" src="https://drive.google.com/file/d/1YK-Mbp5-tR_GoYn7Z3xR_ujv8lCx7wU6/preview" width="640" height="480" allow="autoplay"></iframe>
      <center><p>Joystick</p></center>
    </div>
  </div>
</div>

		</button>
		
		<center>
		<div class="container">
          <div class="row">
            <div class="col">
              <div class="widget-1">
				<a target="_blank" style="text-decoration: none; color: white;" href="https://drive.google.com/file/d/110iz2RUOh3JASC71Uhfj3cK3pX4TvhmB/view?usp=sharing"><h3>AIA</h3></a>
		      </div>
            </div>
            <div class="col">
              <div class="widget-1">
				<a  target="_blank" style="text-decoration: none; color: white;" href="https://drive.google.com/file/d/1aDLcPxA3u_goVzaMT0aMgYkknyzPoM5m/view?usp=sharing"><h3>INO</h3></a>
		      </div>
            </div>
            <div class="col">
              <div class="widget-2">
				<a   target="_blank" style="text-decoration: none; color: white;" href="	https://drive.google.com/file/d/1i0ZEr8vqpump0jOi-LOfpU1VNOBObWnu/view?usp=sharing"><h3>APK</h3></a>
		       </div>
            </div>
          </div>
		</div>
		</center>
		
		</main>
		
		
		
		<aside class="sidebar">
		<section class="section-side">
				<div class="galerie">
				  <div class="photos"><img src=" ../Images/mega1.jpeg" alt=""></div>
				  <div class="photos"><img src=" ../Images/mega2.jpeg" alt=""></div>
				  <div class="photos"><img src=" ../Images/mega3.jpeg" alt=""></div>
				  <div class="photos"><img src=" ../Images/mega4.jpeg" alt=""></div>
				  <div class="photos"><img src=" ../Images/mega5.jpeg" alt=""></div>
				  <div class="photos"><img src=" ../Images/mega6.jpeg" alt=""></div>
				  <div class="photos"><img src=" ../Images/mega7.jpeg" alt=""></div>
				  <div class="photos"><img src=" ../Images/mega8.jpeg" alt=""></div>
				  <div class="photos"><img src=" ../Images/mega9.jpeg" alt=""></div>
				</div>
			  </section>
		</aside>
		
		
		
		<footer class="footer">
        <div class="names">
				<div class="left">
				<h2>Estudiantes:</h2>
				<ul>
					<li>Jhasley Nathalia Pinzón Saíz</li>
					<li>Faiber Rivera Mateus</li>
					<li>Andrés Santiago Jiménez Guzmán</li>
				</ul>
				</div>
				<div class="right">
					<img src="https://image.flaticon.com/icons/png/512/3662/3662920.png" alt="" width="150px">
				</div>
				
			</div>
		<div class="new_footer_top">
                <div class="footer_bg">
                    <div class="footer_bg_one"></div>
                    <div class="footer_bg_two"></div>
                </div>
            </div>
            <div class="footer_bottom">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-6 col-sm-7">
                            <p class="mb-0 f_400">© NOTGRAVITY 2021 All rights reserved.</p>
                        </div>
                    </div>
                </div>
            </div>
		</footer>
	</div>
<button class="arrow-up">
	<i class="fa fa-chevron-up"></i>
  </button>
  <script src="../js/arrowup.js"></script>
    <script>
		const container = document.getElementById('container')
		const img = document.querySelector('.Img1')

container.addEventListener('mousemove', (e) =>{
    const x = e.clientX - e.target.offsetLeft
    const y = e.clientY - e.target.offsetTop

    console.log(x, y)
    img.style.transformOrigin = `${x}px ${y}px`
    img.style.transform = 'scale(2)'

})
	</script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    
   
</body>
</html>