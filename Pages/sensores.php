<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"> 
	<link rel="stylesheet" href="../css/estiloconexiones.css">
	<link rel="stylesheet" href="../css/grafica.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="icon" href="https://image.flaticon.com/icons/png/512/4669/4669453.png">
    <title>Sensores</title>
</head>
<body>
      
<?php 
   /* $mysqli = new mysqli("localhost", "id17124430_santi1923", "Alfaveto1923*", "id17124430_datoscar");
    if ($mysqli->connect_errno) {
        echo "Fallo al conectar a MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
    }
    
    $mysqli->close();*/
    ?>

<div class="contenedor">
		<header class="header">
        <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark">
				<div class="container-fluid">
				  <img src="https://image.flaticon.com/icons/png/512/4212/4212526.png" alt="" width="40px" style="margin-right:20px;" class="d-inline-block align-text-top">
				  <a class="navbar-brand">Sensores</a>
				  <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				  </button>
				  <div class="collapse navbar-collapse" id="navbarNavDropdown">
					<ul class="navbar-nav">
					  <li class="nav-item">
						<a class="nav-link" aria-current="page" href="../index.php">Inicio</a>
					  </li>
					  <li class="nav-item">
						<a class="nav-link" href="../Pages/conexiones.php">Conexiones</a>
					  </li>
					  <li class="nav-item">
						<a class="nav-link active"  href="../Pages/sensores.php">Sensores</a>
					  </li>
					  
					  <li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle"  id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
						  Montajes
						</a>
						<ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
						  <li><a class="dropdown-item" href="../Pages/MontajeUno.php">Montaje Arduino Uno</a></li>
						  <li><a class="dropdown-item" href="../Pages/MontajeMega.php">Montaje Arduino Mega</a></li>
						</ul>
					  </li>
                      <li class="nav-item">
						<a class="nav-link" href="../Pages/controles.php">Control Web</a>
					  </li>

					</ul>
				  </div>
				</div>
			  </nav>
		</header>
		<main class="contenido">
		  		        
		     <script src="../code/highcharts.js"></script>
<script src="../code/exporting.js"></script>
<script src="../code/export-data.js"></script>
<script src="../code/accessibility.js"></script>

<figure class="highcharts-figure">
    <div id="container"></div>
</figure>
		<script type="text/javascript">
Highcharts.chart('container', {
    chart: {
        type: 'line'
    },
    title: {
        text: 'Temperatura y Humedad (Historico)'
    },
    subtitle: {
       // text: 'Source: WorldClimate.com'
    },
    xAxis: {
         categories: [
             <?php
                    $mysqli = new mysqli("localhost", "id17124430_santi1923", "Alfaveto1923*", "id17124430_datoscar");
                    if ($mysqli->connect_errno) {
                    echo "Fallo al conectar a MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
                    }
                
                $sql="SELECT datosensor.Hora FROM datosensor";
                $consulta=mysqli_query($mysqli,$sql);
                while($dato = mysqli_fetch_array($consulta)) {
                    echo "'".$dato['Hora']."'";
                    echo  ",";
                }
                $mysqli->close();
                ?>
             ]
    },
    yAxis: {
        title: {
            //text: 'Temperatura (°C)'
            
            
        }
    },
    plotOptions: {
        line: {
            dataLabels: {
                enabled: true
            },
            enableMouseTracking: false
        }
    },
    series: [{
        name: 'Temperatura ºC',
        data: [
        <?php
                    $mysqli = new mysqli("localhost", "id17124430_santi1923", "Alfaveto1923*", "id17124430_datoscar");
                    if ($mysqli->connect_errno) {
                    echo "Fallo al conectar a MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
                    }
                
                $sql="SELECT datosensor.Temperatura FROM datosensor";
                $consulta=mysqli_query($mysqli,$sql);
                while($dato = mysqli_fetch_array($consulta)) {
                    echo $dato['Temperatura'];
                    echo  ",";
                }
                $mysqli->close();
                ?>]
    }, {
        name: 'Humedad %',
        data: [
            <?php
                    $mysqli = new mysqli("localhost", "id17124430_santi1923", "Alfaveto1923*", "id17124430_datoscar");
                    if ($mysqli->connect_errno) {
                    echo "Fallo al conectar a MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
                    }
                
                $sql="SELECT datosensor.Humedad FROM datosensor";
                $consulta=mysqli_query($mysqli,$sql);
                while($dato = mysqli_fetch_array($consulta)) {
                    echo $dato['Humedad'];
                    echo  ",";
                }
                $mysqli->close();
                ?>]
    }]
});
		</script>


<script src="../code/highcharts.js"></script>
<script src="../code/modules/exporting.js"></script>
<script src="../code/modules/export-data.js"></script>
<script src="../code/modules/accessibility.js"></script>

<figure class="highcharts-figure">
    <div id="container"></div>
    <p class="highcharts-description">
        Chart showing data updating every second, with old data being removed.
    </p>
</figure>



		<script type="text/javascript">
Highcharts.chart('container', {
    chart: {
        type: 'spline',
        animation: Highcharts.svg, // don't animate in old IE
        marginRight: 10,
        events: {
            load: function () {

                // set up the updating of the chart each second
                var series = this.series[0];
                setInterval(function () {
                    var x = (new Date()).getTime(), // current time
                        y = Math.random();
                    series.addPoint([x, y], true, true);
                }, 1000);
            }
        }
    },

    time: {
        useUTC: false
    },

    title: {
        text: 'Detector de objetos (Sensores UltraSonido)'
    },

    accessibility: {
        announceNewData: {
            enabled: true,
            minAnnounceInterval: 15000,
            announcementFormatter: function (allSeries, newSeries, newPoint) {
                if (newPoint) {
                    return 'New point added. Value: ' + newPoint.y;
                }
                return false;
            }
        }
    },

    xAxis: {
        type: 'datetime',
        tickPixelInterval: 150
    },

    yAxis: {
        title: {
            text: 'Value'
        },
        plotLines: [{
            value: 0,
            width: 1,
            color: '#808080'
        }]
    },

    tooltip: {
        headerFormat: '<b>{series.name}</b><br/>',
        pointFormat: '{point.x:%Y-%m-%d %H:%M:%S}<br/>{point.y:.2f}'
    },

    legend: {
        enabled: false
    },

    exporting: {
        enabled: false
    },

    series: [{
        name: 'Random data',
        data: (
            <?php
                    $mysqli = new mysqli("localhost", "id17124430_santi1923", "Alfaveto1923*", "id17124430_datoscar");
                    if ($mysqli->connect_errno) {
                    echo "Fallo al conectar a MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
                    }
                
                $sql="SELECT datodistancia.Deteccion FROM datodistancia";
                $consulta=mysqli_query($mysqli,$sql);
                while($dato = mysqli_fetch_array($consulta)) {
                    echo $dato['Deteccion'];
                    echo  ",";
                }
                $mysqli->close();
                ?>)
    }]
});
		</script>

        <table class="table table-dark table-striped table-hover">
            <thead>
                <tr>
                    <th scope="col">#</th>
                   <th scope="col">Temperatura</th> 
                   <th scope="col">Humedad</th>
                    <th scope="col">Hora</th>
                    
                </tr>
            </thead>
            <tbody>
                <?php
                    $mysqli = new mysqli("localhost", "id17124430_santi1923", "Alfaveto1923*", "id17124430_datoscar");
                    if ($mysqli->connect_errno) {
                    echo "Fallo al conectar a MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
                    }
                
                $sql="SELECT datosensor.ID,datosensor.Temperatura,datosensor.Humedad,datosensor.Time FROM datosensor";
                $consulta=mysqli_query($mysqli,$sql);
                while($dato = mysqli_fetch_array($consulta)) {
                ?>
                <tr>
                     <th scope="row"> <?php echo $dato['ID']; ?> </th>
                     <td> <?php echo $dato['Temperatura']; ?> </td>
                     <td> <?php echo $dato['Humedad']; ?> </td>
                     <td> <?php echo $dato['Time']; ?> </td>
                </tr> 
            
                <?php    
                    }            
                $mysqli->close();
            ?>
            </tbody>
                </table>
            <table class="table table-dark table-striped table-hover">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Deteccion</th>
                            <th scope="col">Hora</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $mysqli = new mysqli("localhost", "id17124430_santi1923", "Alfaveto1923*", "id17124430_datoscar");
                            if ($mysqli->connect_errno) {
                            echo "Fallo al conectar a MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
                            }
                        
                        $sql="SELECT datodistancia.ID,datodistancia.Deteccion,datodistancia.Tiempo FROM datodistancia";
                        $consulta=mysqli_query($mysqli,$sql);
                        while($dato = mysqli_fetch_array($consulta)) {
                        ?>
                        <tr>
                             <th scope="row"> <?php echo $dato['ID']; ?> </th>
                             <td> <?php echo $dato['Deteccion']; ?> </td>
                             <td> <?php echo $dato['Tiempo']; ?> </td>
                        </tr> 
                    
                        <?php    
                            }            
                        $mysqli->close();
                    ?>
                    </tbody>
                </table>
    
		</main>
		
		
		<footer class="footer">
		<div class="names">
				<div class="left">
				<h2>Estudiantes:</h2>
				<ul>
					<li>Jhasley Nathalia Pinzón Saíz</li>
					<li>Faiber Rivera Mateus</li>
					<li>Andrés Santiago Jiménez Guzmán</li>
				</ul>
				</div>
				<div class="right">
					<img src="https://image.flaticon.com/icons/png/512/3662/3662920.png" alt="" width="150px">
				</div>
				
			</div>
		</footer>
	</div>
<button class="arrow-up">
	<i class="fa fa-chevron-up"></i>
  </button>
  <script src="../js/arrowup.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
</body>
</html>