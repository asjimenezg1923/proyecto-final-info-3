<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"> 
	<link rel="stylesheet" href="../css/montajearduino.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
	<link rel="icon" href="https://img.icons8.com/dotty/452/arduino-uno-board.png">
	
	<title>Montaje Arduino Uno</title>
</head>
<body>
	<div class="contenedor">
		<header class="header">
        <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark">
				<div class="container-fluid">
				  <img src="https://image.flaticon.com/icons/png/512/908/908742.png" alt="" width="40px" style="margin-right:20px;" class="d-inline-block align-text-top">
				  <a class="navbar-brand">Arduino Uno</a>
				  <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				  </button>
				  <div class="collapse navbar-collapse" id="navbarNavDropdown">
					<ul class="navbar-nav">
					  <li class="nav-item">
						<a class="nav-link" aria-current="page" href="../index.php">Inicio</a>
					  </li>
					  <li class="nav-item">
						<a class="nav-link" href="../Pages/conexiones.php">Conexiones</a>
					  </li>
					  <li class="nav-item">
						<a class="nav-link" href="../Pages/sensores.php">Sensores</a>
					  </li>
					  
					  <li class="nav-item dropdown">
						<a class="nav-link active dropdown-toggle" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
						  Montajes
						</a>
						<ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
						  <li><a class="dropdown-item" href="../Pages/MontajeUno.php">Montaje Arduino Uno</a></li>
						  <li><a class="dropdown-item" href="../Pages/MontajeMega.php">Montaje Arduino Mega</a></li>
						</ul>
					  </li>
					  <li class="nav-item">
						<a class="nav-link" href="../Pages/controles.php">Control Web</a>
					  </li>
					</ul>
				  </div>
				</div>
			  </nav>
		</header>
		<main class="contenido">
                <div class="montaje">
                    <center><h1>Montaje Arduno Uno</h1>
                    <div id="container">
                        <img src="../Images/Esquema1.jpg" alt="" class="Img1">
                    </div>
                    <p>Plano de Montaje</p></center>
                    <div class="materiales">
						<div class="ContenedorMateriales">
						<div class="contenedor-img">
								<img src="https://png.pngitem.com/pimgs/s/677-6770060_sunfounder-uno-r3-arduino-uno-hd-png-download.png" >
							<div class="centrado">Placa Arduino Uno<br> Precio: $25.500</div>
							</div>
							<div class="contenedor-img">
								<img src="https://http2.mlstatic.com/D_NQ_NP_945220-MCO43955512128_102020-O.jpg" >
							<div class="centrado">Puente H L298N<br>  Precio: $9.500</div>
							</div>
							<div class="contenedor-img">
								<img src="https://www.geekfactory.mx/wp-content/uploads/2014/04/motorreductor-con-llanta-de-goma.jpg" >
							<div class="centrado">Motores Dc con llanta <br> Precio: $9.000 c/u</div>
							</div>
							<div class="contenedor-img">
								<img src="https://electronilab.co/wp-content/uploads/2013/07/A1.jpg" >
							<div class="centrado">Ultrasonido HC-04 <br> Precio: $7.000</div>
							</div>
							<div class="contenedor-img">
								<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQJ_ntoIy0dTVyCmthyhEEr3WBRLGRgDjXL2f1hlvl0HTElK-gpMu_WzxQDVayQUVgh5OtpQaI&usqp=CAc" />
							<div class="centrado">Modulo Bluetooth HC-05<br>  Precio: $22.500</div>
							</div>
							<div class="contenedor-img">
								<img src="https://http2.mlstatic.com/D_NQ_NP_884012-MCO32888438203_112019-O.jpg" >
							<div class="centrado">Pila lipo 3000 mA a 7.4v<br> Precio: $42.500</div>
							</div>
							<div class="contenedor-img">
								<img src="https://http2.mlstatic.com/D_NQ_NP_782657-MCO31539110264_072019-O.jpg" >
							<div class="centrado">Modulo de carga bateria lipo<br> Precio: $15.000</div>
							</div>
							<div class="contenedor-img">
								<img src="https://images-na.ssl-images-amazon.com/images/I/71ZJc50ZZLL._AC_SL1500_.jpg" >
							<div class="centrado">Cargador 12V a 3A<br>  Precio: $15.900</div>
							</div>
							<div class="contenedor-img">
								<img src="https://uelectronics.com/wp-content/uploads/2017/08/Leds-5-mm-difuso-V1.jpg" >
							<div class="centrado">Leds de colores <br>  Precio:$300 c/u</div>
							</div>
							<div class="contenedor-img">
								<img src="https://sandorobotics.com/wp-content/uploads/2017/09/kit-40-jumpers-de-20cm-macho-hembra-3.jpg" >
							<div class="centrado">Jumpers <br>  Precio: $7.000 <br>40 unidades</div>
							</div>
							<div class="contenedor-img">
								<img src="https://images-na.ssl-images-amazon.com/images/I/51kAHG%2BhlEL._SX342_.jpg">
							<div class="centrado">Resistencias 330 ohms <br> Precio:$100 c/u</div>
							</div>
                    </div>
            </div>
            <iframe src=https://create.arduino.cc/editor/not_gravity/38459542-43e7-41cd-9ffa-cb6b421bd070/preview?embed style="height:510px;width:100%;margin:10px 0" frameborder=0></iframe>
            
            
<div class="container">
  <div class="row">
    
    <div class="col">
         <center>
     <iframe src="https://drive.google.com/file/d/1YdmRuc_9k6sG-r4GG8avIkgCA1bEIyZO/preview" width="340" height="280" allow="autoplay"></iframe>
     <p>Joystick</p></center>
    </div>
    
  </div>
</div>
            
            
		<div class="container">
          <div class="row">
            <div class="col">
              <div class="widget-1">
				<a target="_blank" style="text-decoration: none; color: white;" href="https://drive.google.com/file/d/110iz2RUOh3JASC71Uhfj3cK3pX4TvhmB/view?usp=sharing"><h3>AIA</h3></a>
		      </div>
            </div>
            <div class="col">
              <div class="widget-1">
				<a  target="_blank" style="text-decoration: none; color: white;" href="https://drive.google.com/file/d/10VDszcSkFUv0tY4jpkGwxYwrvLKa7svs/view?usp=sharing"><h3>INO</h3></a>
		      </div>
            </div>
            <div class="col">
              <div class="widget-2">
				<a   target="_blank" style="text-decoration: none; color: white;" href="	https://drive.google.com/file/d/1i0ZEr8vqpump0jOi-LOfpU1VNOBObWnu/view?usp=sharing"><h3>APK</h3></a>
		       </div>
            </div>
          </div>
		</div>
		</center>
		
		
		</main>
		<aside class="sidebar">
        <section class="section-side">
				<div class="galerie">
	              <div class="photos"> <img src="../Images/mo10.jpg" alt=""></div>
				  <div class="photos"> <img src="../Images/mo11.jpg" alt=""></div>
				  <div class="photos"> <img src="../Images/mo3.jpg" alt=""></div>
				  <div class="photos"> <img src="../Images/mo4.jpg" alt=""></div>
				  <div class="photos"> <img src="../Images/mo6.jpg" alt=""></div>
				  <div class="photos"> <img src="../Images/mo7.jpg" alt=""></div>
				  <div class="photos"> <img src="../Images/mo8.jpg" alt=""></div>
				  <div class="photos"> <img src="../Images/mo9.jpg" alt=""></div>

				</div>
			  </section>
		</aside>
		
		<footer class="footer">
        <div class="names">
				<div class="left">
				<h2>Estudiantes:</h2>
				<ul>
					<li>Jhasley Nathalia Pinzón Saíz</li>
					<li>Faiber Rivera Mateus</li>
					<li>Andrés Santiago Jiménez Guzmán</li>
				</ul>
				</div>
				<div class="right">
					<img src="https://image.flaticon.com/icons/png/512/3662/3662920.png" alt="" width="150px">
				</div>
				
			</div>
		<div class="new_footer_top">
                
                <div class="footer_bg">
                    <div class="footer_bg_one"></div>
                    <div class="footer_bg_two"></div>
                </div>
            </div>
            <div class="footer_bottom">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-6 col-sm-7">
                            <p class="mb-0 f_400">© NOTGRAVITY 2021 All rights reserved.</p>
                        </div>
                    </div>
                </div>
            </div>
		</footer>
	</div>
<button class="arrow-up">
	<i class="fa fa-chevron-up"></i>
  </button>
  <script src="../js/arrowup.js"></script>
    <script>
		const container = document.getElementById('container')
		const img = document.querySelector('.Img1')

container.addEventListener('mousemove', (e) =>{
    const x = e.clientX - e.target.offsetLeft
    const y = e.clientY - e.target.offsetTop

    console.log(x, y)
    img.style.transformOrigin = `${x}px ${y}px`
    img.style.transform = 'scale(2)'

})
	</script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>

</body>
</html>