	window.onload = function() {
	
	// Variables
	var arrowUp = document.querySelector('.arrow-up');
	var intervalId = 0;
	
	// Functions
	function toggleArrow(e) {

		if (window.scrollY >= 100) {
			arrowUp.classList.add('is-block');

			setTimeout(function() { 
				 arrowUp.classList.add('is-opacity');
			}, 10);

			} else {
				arrowUp.classList.remove('is-opacity'); 
		}

	}

	function scrollStep() {
			if (window.pageYOffset === 0) {
					clearInterval(intervalId);
			}
			window.scroll(0, window.pageYOffset - 50);
	}

	function scrollToTop() {
			intervalId = setInterval(scrollStep, 8.36);
	}


	// Event listeners
	arrowUp.addEventListener('click', scrollToTop);
	window.addEventListener('scroll', toggleArrow);
	
}