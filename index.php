<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"> 
	<link rel="stylesheet" href="css/estilos.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="icon" href="https://image.flaticon.com/icons/png/512/3388/3388852.png">
    <title>Car Control</title>
</head>
<body>
<div class="contenedor">
		<header class="header">
        <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark">
				<div class="container-fluid">
				  <img src="https://image.flaticon.com/icons/png/512/2288/2288026.png" alt="" width="40px" style="margin-right:20px;" class="d-inline-block align-text-top">
				  <a class="navbar-brand" href="#">CAR CONTROL</a>
				  <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				  </button>
				  <div class="collapse navbar-collapse" id="navbarNavDropdown">
					<ul class="navbar-nav">
					  <li class="nav-item">
						<a class="nav-link active" aria-current="page" href="../index.php">Inicio</a>
					  </li>
					  <li class="nav-item">
						<a class="nav-link" href="Pages/conexiones.php">Conexiones</a>
					  </li>
					  <li class="nav-item">
						<a class="nav-link" href="Pages/sensores.php">Sensores</a>
					  </li>
					  
					  <li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
						  Montajes
						</a>
						<ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
						  <li><a class="dropdown-item" href="Pages/MontajeUno.php">Montaje Arduino Uno</a></li>
						  <li><a class="dropdown-item" href="Pages/MontajeMega.php">Montaje Arduino Mega</a></li>
						</ul>
					  </li>

            <li class="nav-item">
						<a class="nav-link" href="Pages/controles.php">Control Web</a>
					  </li>

					</ul>
				  </div>
				</div>
			  </nav>
		</header>
		<main class="contenido">
		    
		    
		     <h2 class="subtitulo">Control de un carro</h2>
			<p align="justify">
				El presente proyecto consiste en controlar un carro de cuatro ruedas y un segundo  vehículo de  tres ruedas por medio de una aplicación, que a su vez se enlaza a la página web (carcontrol2021), con el fin de obtener ciertos datos y realizar algunos movimientos. Para la realización del mismo se implementaron varios sensores, tales como los son el sensor de temperatura y humedad, sensor de distancia y sensor de iluminación y diferentes módulos como el modulo puente H (L298N) y el modulo Bluetooth, junto con el correspondiente arduino para el manejo del robot a controlar, ya sea Arduino Mega para el carro de 4 ruedas o Arduino Uno para el carro de 3 ruedas.
				<br><br>

				En la aplicación (Car Control) se integra un manejo por voz y por joystick, se pueden observar los datos de los sensores de temperatura y humedad de manera instantánea y finalmente se cuenta con un panel de configuración en donde se indica la palabra del control por voz y su correspondiente acción la cual corresponde a los números 1 (adelante), 2 (Atrás), 3 (izquierda), 4 (derecha) y 5 (parar).

				<br><br>

				Por ultimo en la pagina web se puede observar que al inicio en la pagina principal se cuenta con una galeria con algunas imagenes del proyecto, una grafica de los datos de temperatura y humedad, y los ultimos estados de conexion de la aplicacion y datos del sensor, datos los cuales se pueden observar el historial completo en las correspondientes pestañas de conexiones y sensores, de igual manera se cuenta con una pestaña de montajes que encierra tanto el montaje con el Arduino, Uno como el montaje con el Arduino Mega en donde se presentan los correspondientes planos del montaje, imagenes del proyecto, codigo del Arduino y los documentos .aia y .ino para descargar, finalmente se encuentra la pestaña de control web en donde se permite controlar el carro remotamente oprimiendo alguna de las teclas que se indican allí.
			</p>
		    <br>
		   
		    <br>
		    
		    <section class="gallery" id="portafolio">
				<div class="contenedor2">
				  <h2 class="subtitulo">Galería</h2>
				  <div class="contenedor-galeria">
					<img src=" ../Images/mega1.jpeg" alt="" class="img-galeria">					
					<img src=" ../Images/mega4.jpeg"         alt="" class="img-galeria">
					<img src=" ../Images/mega6.jpeg" alt="" class="img-galeria">
					<img src=" ../Images/mega9.jpeg" alt="" class="img-galeria">
					<img src=" ../Images/Esquema2.jpg" alt="" class="img-galeria">
					<img src=" ../Images/mo7.jpg" alt="" class="img-galeria">
					<img src=" ../Images/mo9.jpg" alt="" class="img-galeria">
					<img src=" ../Images/mo3.jpg" alt="" class="img-galeria">
					<img src=" ../Images/Esquema1.jpg" alt="" class="img-galeria">
				  </div>
				</div>
			  </section>
			  
			  <section class="imagen-light">
				<i class="fas fa-times close"></i>
				<img src="./img/img-3.jpg" alt="" class="agregar-imagen">
			  </section>
              
              
  <div class="container">
  <div class="row">
    <div class="col">
      
    </div>
    <div class="col" >
      <iframe style="border-radius: 20px;" src="https://drive.google.com/file/d/1f1pH2_qNB2tl5dGkcybGbtwo7FkTArHa/preview" width="640" height="480" allow="autoplay"></iframe>
    </div>
    <div class="col">
      
    </div>
  </div>
</div>
  
		    
		        
           
			 <script src="code/highcharts.js"></script>
<script src="code/exporting.js"></script>
<script src="code/export-data.js"></script>
<script src="code/accessibility.js"></script>



<figure class="highcharts-figure">
    <div id="container"></div>
    <p class="highcharts-description">
        A continuacion se pueden observar los ultimos registros obtenidos por los sensores y estados de conexion
    </p>
</figure>




		<script type="text/javascript">
Highcharts.chart('container', {
    chart: {
        type: 'line'
    },
    title: {
        text: 'Temperatura y Humedad (Historico)'
    },
    subtitle: {
       // text: 'Source: WorldClimate.com'
    },
    xAxis: {
         categories: [
             <?php
             
                $mysqli = new mysqli("localhost", "id17124430_santi1923", "Alfaveto1923*", "id17124430_datoscar");
                    if ($mysqli->connect_errno) {
                    echo "Fallo al conectar a MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
                    }
                
                $sql="SELECT datosensor.Hora FROM datosensor";
                $consulta=mysqli_query($mysqli,$sql);
                while($dato = mysqli_fetch_array($consulta)) {
                    echo "'".$dato['Hora']."'";
                    echo  ",";
                    
                }
                $mysqli->close();
                ?>
             ]
    },
    yAxis: {
        title: {
            //text: 'Temperatura (°C)'
            
            
        }
    },
    plotOptions: {
        line: {
            dataLabels: {
                enabled: true
            },
            enableMouseTracking: false
        }
    },
    series: [{
        name: 'Temperatura',
        data: [
        <?php
                    $mysqli = new mysqli("localhost", "id17124430_santi1923", "Alfaveto1923*", "id17124430_datoscar");
                    if ($mysqli->connect_errno) {
                    echo "Fallo al conectar a MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
                    }
                
                $sql="SELECT datosensor.Temperatura FROM datosensor";
                $consulta=mysqli_query($mysqli,$sql);
                while($dato = mysqli_fetch_array($consulta)) {
                    echo $dato['Temperatura'];
                    echo  ",";
                }
                $mysqli->close();
                ?>]
    }, {
        name: 'Humedad%',
        data: [
            <?php
                    $mysqli = new mysqli("localhost", "id17124430_santi1923", "Alfaveto1923*", "id17124430_datoscar");
                    if ($mysqli->connect_errno) {
                    echo "Fallo al conectar a MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
                    }
                
                $sql="SELECT datosensor.Humedad FROM datosensor";
                $consulta=mysqli_query($mysqli,$sql);
                while($dato = mysqli_fetch_array($consulta)) {
                    echo $dato['Humedad'];
                    echo  ",";
                }
                $mysqli->close();
                ?>]
    }]
});
		</script>
            <table class="table table-dark table-striped">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col-2">Ultimo Estado</th>
                    <th scope="col">Hora</th>
                    
                </tr>
            </thead>
            <tbody>
                <?php
                    $numero="";
                    $comando="";
                    $hora="";
                    $mysqli = new mysqli("localhost", "id17124430_santi1923", "Alfaveto1923*", "id17124430_datoscar");
                    if ($mysqli->connect_errno) {
                    echo "Fallo al conectar a MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
                    }
                
                $sql="SELECT ARDUINO.ID,ARDUINO.comando,ARDUINO.hora FROM ARDUINO";
                $consulta=mysqli_query($mysqli,$sql);
                while($dato = mysqli_fetch_array($consulta)) {
                        $numero=$dato['ID'];
                        $comando=$dato['comando'];
                        $hora=$dato['hora'];
                    }
                ?>
                <tr>
                    <th scope="row"><?php echo $numero ?></th>
                    <td><?php echo $comando ?></td>
                    <td><?php echo $hora ?></td>
                    
                </tr>
            </tbody>
            </table>
            <?php
            
            $mysqli->close();
            ?>
            
            <table class="table table-dark table-striped">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Ultimo Estado <br> Temperatura</th>
                    <th scope="col">Ultimo Estado <br> Humedad</th>
                    <th scope="col">Hora</th>
                    
                </tr>
            </thead>
            <tbody>
                <?php
                    $numero="";
                    $tem="";
                    $hum="";
                    $time="";
                    $mysqli = new mysqli("localhost", "id17124430_santi1923", "Alfaveto1923*", "id17124430_datoscar");
                    if ($mysqli->connect_errno) {
                    echo "Fallo al conectar a MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
                    }
                
                $sql="SELECT datosensor.ID,datosensor.Temperatura,datosensor.Humedad,datosensor.Time FROM datosensor";
                $consulta=mysqli_query($mysqli,$sql);
                while($dato = mysqli_fetch_array($consulta)) {
                        $numero=$dato['ID'];
                        $tem=$dato['Temperatura'];
                        $hum=$dato['Humedad'];
                        $time=$dato['Time'];
                    }
                ?>
                <tr>
                    <th scope="row"><?php echo $numero ?></th>
                    <td><?php echo $tem ?></td>
                    <td><?php echo $hum ?></td>
                    <td><?php echo $time ?></td>
                    
                </tr>
            </tbody>
            </table>
            <?php
            
            $mysqli->close();
            ?>
            
            
		</main>
		
		<footer class="footer">
			<div class="names">
				<div class="left">
				<h2>Estudiantes:</h2>
				<ul>
					<li>Jhasley Nathalia Pinzón Saíz</li>
					<li>Faiber Rivera Mateus</li>
					<li>Andrés Santiago Jiménez Guzmán</li>
				</ul>
				</div>
				<div class="right">
					<img src="https://image.flaticon.com/icons/png/512/3662/3662920.png" alt="" width="150px">
				</div>
				
			</div>
		<div class="new_footer_top">
                
                <div class="footer_bg">
                    <div class="footer_bg_one"></div>
                    <div class="footer_bg_two"></div>
                </div>
            </div>
            <div class="footer_bottom">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-6 col-sm-7">
                            <p class="mb-0 f_400">© NOTGRAVITY 2021 All rights reserved.</p>
                        </div>
                    </div>
                </div>
            </div>
		</footer>
	</div>
	
	<button class="arrow-up">
	<i class="fa fa-chevron-up"></i>
  </button>
  <script src="js/arrowup.js"></script>
<script src="js/gallery.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>


</body>
</html>